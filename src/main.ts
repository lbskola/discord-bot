import Discord from 'discord.js'
import token from './config.json'
const client = new Discord.Client()
import * as math from 'mathjs'

const prefix = '!'

client.on('ready', () => {
  console.log(`Logged in as ${client.user!.tag}`)
})

client.on('message', (msg) => {
  if (!msg.content.startsWith(prefix)) return false
  if (msg.author.bot) return false
  if (!msg.guild) return false

  const command = msg.content.slice(prefix.length)

  if (command.startsWith('say ')) {
    const content = getArgs(command, 'say')
    if (content.match(/(@everyone)/g)) {
      msg.channel.send('No @.everyone is allowed.')
      return false
    }
    msg.channel.send(content)
  } else if (command.startsWith('calc ')) {
    try {
      const res = math.evaluate(getArgs(command, 'calc'))
      if (res == 69) {
        msg.channel.send('_nice_')
        return false
      }
      msg.channel.send(res)
    } catch (error) {
      msg.channel.send(`An error occured, please try again.\n${error}`)
    }
  }
})

function getArgs(command: string, invoker: string): string {
  return command.slice(invoker.length).trim()
}

client.login(token)
